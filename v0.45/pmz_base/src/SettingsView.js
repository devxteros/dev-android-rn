/**
 * Base React Native App Pmz
 * https://github.com/facebook/react-native
 */

import React, { Component } from 'react';
import {
  View,
  Text  
} from 'react-native';

import { styles } from './styles';
import { MESSAGES } from './config/messages'

export default class SettingsView extends Component {

	static navigationOptions = {
		title: MESSAGES.SETTINGS,
	};


  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          {MESSAGES.SETTINGS}
        </Text>
      </View>
    );
  }
}