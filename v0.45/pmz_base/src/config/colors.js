/* 
Material Design is a unified system that combines theory, resources, and tools for crafting digital experiences.
COLOR TOOL
Create, share, and apply color palettes to your UI, as well as measure the accessibility level of any color combination.
Visit: https://material.io/color
*/
export const COLOR = {
	PRIMARY:'#0277bd',
	PRIMARY_LIGHT:'#58a5f0',
	PRIMARY_DARK:'#004c8c',
	SECONDARY:'#8bc34a',
	SECONDARY_LIGHT:'#bef67a',
	SECONDARY_DARK:'#5a9216',
	TEXT_PRIMARY:'#ffffff',
	TEXT_SECONDARY:'#424242',	
}