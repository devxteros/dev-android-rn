import React, { Component } from 'react';
import {ActivityIndicator,Text,View}from 'react-native'; 
	

export default class Spinner extends Component{
	
    /**
	 * Author Alonso meneses 
	 * initialize parameters default
	 * @param {*} props from instance
	 */
	constructor(props) {
		super(props);      									
		this.state = {
			loading:(this.props.loading)? this.props.loading: true,						
			title:(this.props.text) ? this.props.text : 'Loading...',		
            flexDirection: {
                flexDirection : (this.props.flexDirection) ? this.props.flexDirection :	'row'
            },
            textStyle:{
                fontFamily:(this.props.fontFamily)? this.props.fontFamily:'arial',
				fontWeight:(this.props.fontWeight)? this.props.fontWeight:'normal',
				fontSize:(this.props.fontSize)? this.props.fontSize:15,
				color:(this.props.fontColor)? this.props.fontColor:'black'
            },
            size:(this.props.size)? this.props.size:"small",
            animating: (this.props.animating)? this.props.animating:true
            

		}
	}

    /**
     * return the component
     */
	render(){
		if (this.state.loading){
			return(
				<View style={this.state.flexDirection}>
					<ActivityIndicator
						animating={this.state.animating}
						size={this.state.size}
					/>
					<Text style={this.state.textStyle}>{this.state.title}</Text>
				</View>
			);
		}else{
			return null;
		}
	}

}