import React, { Component } from 'react';
import {  
	Text,
	View,
	TouchableHighlight,
	Button
} from 'react-native';

/**
 * Developed by premize.s.a.s
 * All rights reserved
 * Author Alonso Meneses V.
 * DATE : 20/06/2017
 */

export default class buttonEdit extends Component{
	
	/**
	 * Author Alonso meneses 
	 * initialize parameters default
	 * @param {*} props from instance
	 */
	constructor(props) {
		super(props);      									
		this.state = {
			title:(this.props.title)? this.props.title: 'Title is empty',						
			disabled:(this.props.disabled) ? this.props.disabled : false,			

			backgroundStyle:{
				backgroundColor:(this.props.backgroundColor)? this.props.backgroundColor: 'red',
				alignItems:(this.props.alignItems)? this.props.alignItems:'center',
				borderWidth:(this.props.alignItems)? this.props.alignItems:8,
				borderColor:(this.props.backgroundColor)? this.props.backgroundColor: 'red' 
			},
			fontStyle:{
				fontFamily:(this.props.fontFamily)? this.props.fontFamily:'arial',
				fontWeight:(this.props.fontWeight)? this.props.fontWeight:'bold',
				fontSize:(this.props.fontSize)? this.props.fontSize:15,
				color:(this.props.fontColor)? this.props.fontColor:'white'
			}
		}
	}

	/**
	 * excecute function from instance
	 * @param {*} callback 
	 */
	execute(callback){
		if (this.props.callBack) { 
			callback() 
		}else {
			alert('Function is empty')
		}
		
	}

	/**
	 * return from component
	 **/
	render(){		
		return (											
			<TouchableHighlight  onPress={()=>this.execute(this.props.callBack)} disabled={this.state.disabled} style={this.state.backgroundStyle}>
				<Text style={this.state.fontStyle}>{this.state.title.toUpperCase()}</Text>						
			</TouchableHighlight>						
		);	
	}
}