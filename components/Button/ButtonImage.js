import React, { Component } from 'react';
import { 
    View,
    Button,
    Image,
    TouchableHighlight,
    Text
} from 'react-native';

/**
 * Developed by premize.s.a.s
 * All rights reserved
 * Author Alonso Meneses V.
 * DATE : 20/06/2017
 */

//import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/FontAwesome';
export default class buttonIcon extends Component{
	
	/**
	 * Author Alonso meneses 
	 * initialize parameters default
	 * @param {*} props from instance
	 */
	constructor(props) {
		super(props);      									
		this.state = {
			title:(this.props.title)? this.props.title: 'Title is empty ',
			color: (this.props.color)? this.props.color: 'black',			
			disabled:(this.props.disabled) ? this.props.disabled : false,
			nameIcon:(this.props.nameIcon) ? this.props.nameIcon : 'windows',

            style:{
                width:(this.props.width) ? this.props.width : 150,
                height:(this.props.height) ? this.props.height : 150,

            },
            uri:{
                uri:(this.props.uri)? this.props.uri:'https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAAgSAAAAJDg4MTBhZTM2LWQ2OGEtNGViMy05ZmNjLTBiZDUyZGJlYjUyNA.jpg',
                
            }
		}
	}

	/**
	 * excecute function from instance
	 * @param {*} callback 
	 */
	execute(callback){
		callback();
	}

	/**
	 * return the component
	 */
	render(){	
		return (					
            <TouchableHighlight activeOpacity={.9}  onPress={(this.props.function) ? ()=>this.execute(this.props.function) : ()=>alert('Function is empty')} disabled={this.state.disabled}>
                <Image
                    style={this.state.style}                
                    source={(this.props.uri)? this.state.uri: (this.props.source)?this.props.source:this.state.uri}
                />
            </TouchableHighlight>			
		);			
	}
}