import React, { Component } from 'react';
import { 
    View,
    Button,
    Text,
	TouchableOpacity
} from 'react-native';

/**
 * Developed by premize.s.a.s
 * All rights reserved
 * Author Alonso Meneses V.
 * DATE : 20/06/2017
 */

import Icon from 'react-native-vector-icons/FontAwesome';
export default class buttonIcon extends Component{
	
	/**
	 * Author Alonso meneses 
	 * initialize parameters default
	 * @param {*} props from instance
	 */
	constructor(props) {
		super(props);      									
		this.state = {
			title:(this.props.title)? this.props.title: 'Title is empty ',			
			disabled:(this.props.disabled) ? this.props.disabled : false,
			nameIcon:(this.props.nameIcon) ? this.props.nameIcon : 'windows',
			iconColor: (this.props.iconColor)? this.props.iconColor: 'white',
			iconsize:(this.props.iconsize)? this.props.iconsize: 30,
			iconOrientation:(this.props.iconOrientation) ? this.props.iconOrientation.toUpperCase():null,

			viewStyle:{
				flexDirection: "row",
				backgroundColor: (this.props.backgroundColorCenter)? this.props.backgroundColorCenter: 'black',					
				alignItems: 'center'
			},
			textStyle:{
				fontFamily: (this.props.fontFamily)? this.props.fontFamily:'arial', 
				fontSize: (this.props.fontSize)? this.props.fontSize:15, 
				color:(this.props.fontColor)? this.props.fontColor:'white',
				paddingHorizontal:10
			},
			backgroundStyle:{				
				alignItems:'center',
				borderWidth:(this.props.alignItems)? this.props.alignItems:8,
				borderColor:(this.props.backgroundColor)? this.props.backgroundColor: 'black' 
			}			
		}
	}

	/**
	 * excecute function from instance
	 * @param {*} callback 
	 */
	execute(callback){
		if (this.props.callBack){
			callback();		
		}else{
			alert('Function is empty')
		}
		
	}

	/**
	 * return from component
	 */
	render(){	
		
		/**
		 * icon orientation validation  
		 */
		switch(this.state.iconOrientation){

			case 'RIGHT':
				return (								
					<TouchableOpacity activeOpacity={.5} style={this.state.backgroundStyle} onPress={()=>this.execute(this.props.callBack)} disabled={this.state.button}>  
						<View style={this.state.viewStyle}>           						
							<Text style={this.state.textStyle}>{this.state.title}</Text>   
							<Icon name={this.state.nameIcon} size={this.state.iconsize} color={this.state.iconColor} />
						</View>
					</TouchableOpacity>		
				);

			case 'UP':
				return(
					<TouchableOpacity activeOpacity={.5} style={this.state.backgroundStyle} onPress={()=>this.execute(this.props.callBack)} disabled={this.state.button}>  
						<Icon name={this.state.nameIcon} size={this.state.iconsize} color={this.state.iconColor} />
						<View style={this.state.viewStyle}>           						
							<Text style={this.state.textStyle}>{this.state.title}</Text>   
						</View>
					</TouchableOpacity>
				);

			case 'DOWN':
				return(
					<TouchableOpacity activeOpacity={.5} style={this.state.backgroundStyle} onPress={()=>this.execute(this.props.callBack)} disabled={this.state.button}>  						
						<View style={this.state.viewStyle}>           						
							<Text style={this.state.textStyle}>{this.state.title}</Text>   
						</View>
						<Icon name={this.state.nameIcon} size={this.state.iconsize} color={this.state.iconColor} />
					</TouchableOpacity>
				);

			default:
				return (								
					<TouchableOpacity activeOpacity={.5} style={this.state.backgroundStyle} onPress={()=>this.execute(this.props.callBack)} disabled={this.state.button}>  
						<View style={this.state.viewStyle}>           
							<Icon name={this.state.nameIcon} size={this.state.iconsize} color={this.state.iconColor} />
							<Text style={this.state.textStyle}>{this.state.title}</Text>   
						</View>
					</TouchableOpacity>		
				);	
		}				
	}
}